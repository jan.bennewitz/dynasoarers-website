﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Dynasoarers</title>
<link rel="stylesheet" type="text/css" href="screenstyle.css" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body>
<div class="container">

<div class="titleblock">
    <h1>
        The Dynasoarers</h1>
    <p>
        Hang gliding &amp; Paragliding on the Surf Coast</p>
</div>
<div>
    <ul class="navbar">
        <li><a href="index.shtml" class="nav">Home</a></li>
        <li><a href="sites.shtml" class="nav">Site Guide</a></li>
        <li><a href="membership.shtml" class="nav">Membership</a></li>
        <li><a href="articles.shtml" class="nav">Articles</a></li>
        <li><a href="weather.shtml" class="nav">Weather</a></li>
        <li><a href="http://groups.google.com/group/dynasoarers" class="nav">Forum</a></li>
        <li><a href="pictures.shtml" class="nav">Pictures</a></li>
        <li><a href="contact.shtml" class="nav">Contact</a></li>
    </ul>
</div>
<div class="rightcontainer">
    <div class="rightbox">
        <h2>
            Next Meeting <a href="feed.rss" type="application/rss+xml">
                <img src="feed-icon-14x14.png" alt="RSS feed for the Dynasoarers" style="border-top-style: none;
                    border-right-style: none; border-left-style: none; border-bottom-style: none;
                    left: 0px; top: 2px; position: relative;" /></a>
        </h2>
        <p>
            <a href='mailto:dynasoarers@gmail.com'>Contact us</a> for details of the next meeting.
                </p>
    </div>
    <div class="rightbox linkbox">
        <h2>
            Links</h2>
        <a href="http://www.hgfa.asn.au" title="Hang Gliding Federation of Australia">HGFA</a>
        <a href="http://www.vhpa.org.au" title="Victorian Hang Gliding and Paragliding Association">
            VHPA</a> <a href="http://www.vhpa.org.au/clubs.html" title="Victorian Flying Clubs">
                Victorian Clubs</a>
    </div>
    <div class="rightbox linkbox">
        <h2>
            Resources</h2>
        <p>
            <a href="spion.shtml" title="Spion Site Endorsement details">Spion Site Endorsement
                details</a></p>
				
				<h2>
            Software</h2>
        <p>
		
			<a href="https://play.google.com/store/apps/details?id=com.pteranodon.weatherviews" 
			title="Weather Views Oz"><img src="windsock16.png" alt="windsock image"  />
                Cindi's Android Weather Views application - view multiple weather stations on a page</a> </br>
				
				<a href="http://www.wxtide32.com/" title="Free tide prediction software for Windows">
                    Tides for Windows</a></br> <a href="http://shonkylogic.net/shonkymaps/" title="Free maps for Garmin GPS">
                        Free Garmin maps</a>
        </p>
    </div>
</div>


<div class="content">

    <h2>Stories and Articles</h2>

    <ul>
        <li><a href="coombsy big day out.shtml">Coombsy's Big Day Out</a> - by Geoff Coombs</li>
        <li><a href="coombsy coastal run.shtml">Coombsy's Coastal Run (Bell's to Apollo Bay)</a> - by Geoff Coombs</li>
        <li><a href="who needs the morning glory.shtml">Who needs the Morning Glory</a> - Geoff Coombs</li>
        <li><a href="darren 1st cross country.shtml">My first Cross Country</a> - by Darren Brown</li>
        <li><a href="harry 1st one.shtml">My First One</a> - Harry Buckle</li>
        <li><a href="borderlands.shtml">A Flight to the Borderlands</a> - a classic - 1998 by Paul Gazis</li>
        <li><a href="http://www.hgfa.asn.au/learning/learning.htm">Learning to Hang Glide</a> (on the HGFA Website) - answers questions you may have about learning to hang glide.</li>
        <li><a href="http://www.aerialpursuits.com/misc/vichg.htm">Early Victorian Hang Gliding</a> by John Reynoldson</li>
        <li><a href="rules for flight.shtml">Rules for Flight</a> - Unknown</li>
        <li><a href="hardcore pilot signs.shtml">Sure Fire signs to show that you are a Hard Core Hang Glider/Paraglider Pilot</a> - Unknown</li>
    </ul>


<h2>The coast run - Bells Beach to Apollo Bay</h2>

<p>By Geoff Coombs</p>

<p>Bells Beach to Apollo Bay. For pilots who fly the Victorian coast it's the Holy
Grail of hang gliding. The flying is challenging and the views fantastic. It is
very rarely done. Up until a few years ago less than ten people had ever
succeeded. Now seventeen people have managed the run for a total of thirty-five
times completed. The first guys to make it were Peter Muffet and Angus Walker 
[Angus in fact flew all the way to the Cape Otway lighthouse -Ed.] back in 1990.
Then it was four years before it was done again! The undisputed
king of the coastal run is Ted Remeika who has succeeded an amazing thirteen
times. Sitting in the Aireys Inlet Pub is a trophy documenting all the people
and dates the run has been completed. If you are passing by it's worth a look
and it's a good excuse for a beer. 
</p>

<p><img src="Winki.jpg" alt="" title="Wiki launch - on the way to Apollo Bay" style="float: right; margin-left: 20px; margin-bottom: 15px;" 
/>Why is it so rare? Well, for a start, you need a SE to East wind around 15 to 20
knots which is unusual for the West Coast. For instance, up until a month ago we hadn't had a suitable wind for
more than a year! Secondly, it is
very challenging. For a newcomer, you'd have to fly with a local who has done
it before. After you've bombed along the way a few times and figured out your
mistakes, felt comfortable flying low with limited landing options and coped
with a tail wind, low altitude run along Fairhaven beach, well then, you've
almost made it half way! Of course, then you've got to get past Lorne which is
quite likely to be harder than getting past Fairhaven (but not as scary)!
Thirdly, you have to be there when it's on. And finally, a bit of luck helps!
</p>

<p>The total distance as the crow flies is 73kms. You can fly further, down to Cape
Otway, another 17kms (it's been done) but I don't like your chances of
getting picked up. It's another ¾ hr drive to get you! 
</p>

<p>The launch at Bells Beach (coast faces ESE) is now rated as advanced, not because
the launch is hard, in fact it's one of the easiest around, but because there
is nowhere to land at the bottom, unless its low tide and you want to land on
rocks (it's been done). There is a top landing area but if the wind is strong
(15-20knots) it is a little turbulent and not a big landing area. Basically, in
a strong wind you fly southwest and land at Anglesea or beyond.
</p>

<p><img src="Leigh at Jan Juc.jpg" alt="" title="Leigh above the Jan Juc cliffs" style="float: left; margin-right: 20px; margin-bottom: 15px;" 
/>After launch, you generally fly down to the cliffs near Jan Juc (north), get high
(over 550ft), then fly back and cross Bells Beach itself and on to the cliffs at
South side. Usually, unless it's a very high tide, there is plenty of beach to
land on if you have to, but it's a bit of a walk out. After gaining some height, 
it's down to Pt Addis where you want to get as high as
possible to cross inland (minimum around 900ft) onto the cliffs at Eumeralla
(coast faces SSE). The general consensus these days is to try to cross in lift
and drift down wind onto Eumeralla, maintaining height as much as possible. From
there you fly across to Anglesea and Pt Roadnight lookout. Now, if you've been
patient and flown down wind in buoyant air, you've arrived at Roadnight high
enough to again cross inland to the cliffs at Urquhart Bluff without having to
stop (coast faces S between Roadnight and Urquhart). If you can't do that then
you have to hang around Roadnight until you can climb to a minimum of 600ft. And
I mean minimum, I don't know of anyone who has managed to cross to Urquhart
and stay flying after leaving under 600ft (plenty have been higher and still
landed on the beach). Besides it's 4 ½kms and fairly flat ground, 1000ft
doesn't seem enough! 
</p>

<p>From there you fly along the cliffs to Aireys Inlet Lighthouse (coast faces ESE).
There are very few landing spots, so you need to keep an eye on the wind (wind
lines and white caps). Sometimes the wind can drop off as you fly along the
coast and a few people have been caught out and landed in the water. Luckily, no
one has been hurt.
</p>

<p>Once at the lighthouse area it's important to get as high as you possibly can
(what's new you might ask), anywhere over 400ft, as this is one of the
toughest areas of the flight. As the wind is round to the east the well known
flying area of Fairhaven, or Spion, as most people call it, (coast faces S)
becomes very tough to get past. The wind is between 45degress and 70 degrees
off, sometimes more if it is closer to east. Also the first part of the flight
is over very low sand dunes. Here you are flying with a ground speed around
70-80kms/hr at less than 300ft with an airspeed around trim! If your heart rate
isn't up you must be dead! Also, it's a good idea to have your harness
unzipped just in case you have to do a hurried 180 degree turn back into the
wind to land. Now comes the tricky part. You have to fly along the ridge at
Fairhaven and then turn very sharply back into the wind just before the launch
area, which faces more into wind and generates the best lift you are going to
get. If you skid past launch in the turn then you will struggle back into wind
and around the corner losing height as you go. Believe me, you won't have much
to waste. If you turn too early you won't be in the best lift band and will
struggle to maintain height. If you get it just right you will slowly gain
enough height (500ft) to turn tail wind again and make it down to Cinema Point
3.5kms away (faces ESE). From here you can breathe a sigh of relief and climb
easily, reflecting on how shit scared you were just a few moments ago.
</p>

<p>After climbing to around 800ft you can then cross a small gap onto what we call Big
Hill. Here again you want to get maximum height, over 2000ft if that's
possible. This is the highest point before Lorne, so if you can maintain this
all the way down it will give you the maximum chance of getting onto to Teddys
Lookout, on the other side of Lorne. Now this is where the modern topless
gliders make a big difference. To get onto Teddys you have to push 2kms into a
strong headwind with no lift. Being able to do 80km/hr (or more if you need it)
and still have a reasonable glide is a huge advantage. The Litespeed is fabulous
for this. You have to get the glide right because before Teddys the landings are
ordinary and once there the landing areas are just as bad. I guess you could put
it down if you had to…
</p>

<p><img src="Wye River.jpg" alt="" title="Above Wye River" style="float: right; margin-left: 20px; margin-bottom: 15px;" 
/>Once on Teddys Lookout you can allow yourself a smile because from here on its plain
sailing, compared with what has gone before. The hills are high and it's
usually an easy climb to over 2000ft. The views are spectacular and you have the
time to enjoy them. Cumberland River, Wye River, Kennett River, you slowly pass
them all, watching the tiny cars below, as they snake their way along the Great
Ocean Road. As you approach Cape Patton it's a good idea to climb as high as
possible again as the run into Apollo Bay is more tail wind and the hills are
lower and not as steep. It's still 16kms to go and you don't want to blow it
now.</p>

<p>After passing Cape Patton, you race the cars down past Skenes Creek to Apollo Bay.
It's a great feeling getting there, something special. If you like you can fly
onto Mariners Lookout, the launch site for paragliders and hang gliders, flying
at Apollo Bay. It's a very pretty area, especially on a good day with plenty
of sunshine.
</p>

<p>The best landing area is crosswind on the beach near the Surf Life Saving Club where
you can then walk your glider up onto the grass and pack up in comfort.
</p>

<p>Time for a well earned beer and that's right, somehow you've got to get back!
Hopefully somebody will be kind enough to come and get you. And put up with all
the gloating and the “no shit there I was…” stories, all the way back.
</p>

<p>If you're keen to try this flight keep an eye on the forecast and contact a local
in the area. Try looking on the <a href="contact.shtml">Contact page</a> or look in 
Soaring Australia for contact numbers.
</p>

</div>

<div class="footer">
<div class="right">
    <p>Email <a href="mailto:dynasoarers@gmail.com">Webmaster</a></p>
</div>
</div>


</div>
</body>
</html>
